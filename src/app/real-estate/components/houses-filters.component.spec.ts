import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMaterialModule } from '../../material.module';
import { HousesFiltersComponent } from './houses-filters.component';

describe('HousesFiltersComponent', () => {
  let component: HousesFiltersComponent;
  let fixture: ComponentFixture<HousesFiltersComponent>;
  let cityIdEl: HTMLElement;
  let priceLessThanEl: HTMLElement;
  let onSaleEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [HousesFiltersComponent],
        imports: [MyMaterialModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousesFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    cityIdEl = fixture.nativeElement.querySelector('#cityId');
    priceLessThanEl = fixture.nativeElement.querySelector('#priceLessThan');
    onSaleEl = fixture.nativeElement.querySelector('#onSale');
  });

  it('should create filters form', () => {
    expect(component.filtersForm instanceof FormGroup).toBe(true);
  });

  // TODO(1pts)
  it('form should have cityId control', () => {
      expect(component.filtersForm.controls.cityId instanceof FormControl).toBe(true);
  });

  // TODO(1pts)
  it('should render cityId control', () => {
      expect(cityIdEl === null).toBe(false);
  });

  // TODO(1pts)
  it('form should have priceLessThan control', () => {
      expect(component.filtersForm.controls.priceLessThan instanceof FormControl).toBe(true);
  });

  // TODO(1pts)
  it('should render priceLessThan control', () => {
      expect(priceLessThanEl === null).toBe(false);
  });

  // TODO(1pts)
  it('form should have onSale control', () => {
      expect(component.filtersForm.controls.onSale instanceof FormControl).toBe(true);
  });

  // TODO(1pts)
  it('should render onSale control', () => {
      expect(onSaleEl === null).toBe(false);
  });
});
