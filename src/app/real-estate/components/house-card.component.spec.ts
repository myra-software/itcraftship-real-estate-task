import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MyMaterialModule } from '../../material.module';
import { House } from '../models';
import { HouseCardComponent } from './house-card.component';

describe('HouseCardComponent', () => {
  let component: HouseCardComponent;
  let fixture: ComponentFixture<HouseCardComponent>;
  let houseTitleEl: HTMLElement;
  let houseDescriptionEl: HTMLElement;
  let houseImageEl: HTMLElement;
  let housePriceEl: HTMLElement;
  let houseOnSaleEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [HouseCardComponent],
        imports: [ MyMaterialModule ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseCardComponent);
    component = fixture.componentInstance;

    component.house = { id: 1, title: 'Test House',
        cityId: 2, price: 150, onSale: false,
        description: 'House description',
        image: 'http://via.placeholder.com/150x150'};

    fixture.detectChanges();

    houseTitleEl = fixture.nativeElement.querySelector('.house-title');
    houseDescriptionEl = fixture.nativeElement.querySelector('.house-description');
    houseImageEl = fixture.nativeElement.querySelector('.house-image');
    housePriceEl = fixture.nativeElement.querySelector('.house-price');
  });

  // TODO(1pts)
  it('should render house title', () => {
      expect(String(houseTitleEl.textContent)).toBe(component.house.title);
  });

  // TODO(1pts)
  it('should render card desription', () => {
      expect(String(houseDescriptionEl.textContent)).toBe(component.house.description);
  });

  // TODO(1pts)
  it('should render card image', () => {
      expect(String(houseImageEl.getAttribute('src'))).toBe(component.house.image);
  });

  // TODO(1pts)
  it('should render house price', () => {
      expect(String(housePriceEl.textContent)).toBe(String(component.house.price));
  });

  // TODO(1pts)
  it('should render house onSale if house is on sale', () => {
      // we crete new house with onSale true to test if house is on sale
      component.house = { id: 1, title: 'Test House',
          cityId: 2, price: 150, onSale: true,
          description: 'House description',
          image: 'http://via.placeholder.com/150x150'};
      fixture.detectChanges();
      houseOnSaleEl = fixture.nativeElement.querySelector('.house-on-sale');
      expect(String(houseOnSaleEl.textContent)).not.toBe('');
  });

  // TODO(1pts)
  it('should NOT render house onSale if house is not on sale', () => {
      houseOnSaleEl = fixture.nativeElement.querySelector('.house-on-sale');
      expect(String(houseOnSaleEl.textContent)).toBe('');
  });
});
