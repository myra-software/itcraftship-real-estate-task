import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { City, HouseFilters } from '../models';

/* TODO(5pts): create form controls */
/* TODO(5pts): render form */

@Component({
  selector: 'app-houses-filters',
  template: `
      <mat-expansion-panel (opened)="false">
          <mat-expansion-panel-header>
              <mat-panel-title>Filters</mat-panel-title>
          </mat-expansion-panel-header>

          <form [formGroup]="filtersForm" (ngSubmit)="onSubmit()">
            <mat-grid-list cols="1" rowHeight="60">
                <mat-grid-tile>
                    <mat-form-field>
                        <mat-select id="cityId" formControlName="cityId" placeholder="City">
                            <mat-option [value]="">All</mat-option>
                            <mat-option *ngFor="let city of cities" [value]="city.id">
                                {{ city.name }}
                            </mat-option>
                        </mat-select>
                    </mat-form-field>
                </mat-grid-tile>
                <mat-grid-tile>
                    <mat-form-field class="example-full-width">
                        <input id="priceLessThan" matInput type="number" formControlName="priceLessThan" placeholder="Price less than" />
                    </mat-form-field>
                </mat-grid-tile>
                <mat-grid-tile>
                    <label>On Sale:</label>
                    <mat-radio-group id="onSale" formControlName="onSale">
                        <mat-radio-button value="" checked="true">All</mat-radio-button>
                        <mat-radio-button value="true">Yes</mat-radio-button>
                        <mat-radio-button value="false">No</mat-radio-button>
                    </mat-radio-group>
                </mat-grid-tile>
                <mat-grid-tile>
                    <button mat-raised-button color="primary" type="submit">Search</button>
                </mat-grid-tile>
            </mat-grid-list>
          </form>
      </mat-expansion-panel>
  `
})
export class HousesFiltersComponent {

  filtersForm: FormGroup;

  @Output() filtersChange = new EventEmitter<HouseFilters>();

  @Input()
  set filters(v: HouseFilters) {

    if (typeof v !== 'undefined' && v !== null) {

      const values: HouseFilters = {};
        values.onSale = v.onSale;

      if (typeof v.cityId !== 'undefined') {
          values.cityId = Number(v.cityId);
      }

      if (typeof v.priceLessThan !== 'undefined') {
          values.priceLessThan = Number(v.priceLessThan);
      }

      this.filtersForm.patchValue(values || {});
    }
  }

  @Input() cities: City[];

  constructor(
      private formBuilder: FormBuilder
  ) {
      this.filtersForm = this.formBuilder.group({
          cityId: [''],
          priceLessThan: [''],
          onSale: [''],
      });
  }

  onSubmit() {
    this.filtersChange.emit(this.filtersForm.value);
  }
}
