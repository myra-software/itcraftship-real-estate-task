import { Component, Input, OnInit } from '@angular/core';
import { House } from '../models';

/* TODO(5pts): render houses list */
@Component({
  selector: 'app-houses-list',
  template: `
      <mat-card>
      <table mat-table [dataSource]="houses" class="mat-elevation-z8">
          <ng-container matColumnDef="id">
              <th mat-header-cell *matHeaderCellDef> ID </th>
              <td mat-cell *matCellDef="let element">{{element.id}}</td>
          </ng-container>

          <ng-container matColumnDef="title">
              <th mat-header-cell *matHeaderCellDef> Title </th>
              <td mat-cell *matCellDef="let element" id="title_{{ element.id }}">{{element.title}}</td>
          </ng-container>

          <ng-container matColumnDef="price">
              <th mat-header-cell *matHeaderCellDef> Price </th>
              <td mat-cell *matCellDef="let element">{{element.price}}</td>
          </ng-container>

          <ng-container matColumnDef="onSale">
              <th mat-header-cell *matHeaderCellDef> OnSale </th>
              <td mat-cell *matCellDef="let element">{{element.onSale}}</td>
          </ng-container>

          <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
          <tr mat-row *matRowDef="let row; columns: displayedColumns;" [routerLink]="['.', row.id]"></tr>
      </table>
      </mat-card>
  `,
  styles: []
})
export class HousesListComponent implements OnInit {
  @Input() houses: House[];
  displayedColumns: Array<string>;

  ngOnInit() {
    this.displayedColumns = ['id', 'title', 'price', 'onSale'];
  }
}
