import { Component, Input } from '@angular/core';
import { House } from '../models';

// TODO(5pts): render house

@Component({
  selector: 'app-house-card',
  template: `
  <mat-card>
      <mat-card-title class="house-title">{{ house.title }}</mat-card-title>

      <img class="house-image" mat-card-image src="{{ house.image}}" />

      <mat-card-content>
          <label>Price </label><span class="house-price">{{ house.price }}</span>
      </mat-card-content>

      <mat-card-content>
          <div class="house-on-sale">
            <span *ngIf="house.onSale">On sale</span>
          </div>
      </mat-card-content>
      <mat-card-content>
          <p class="house-description">{{ house.description }}</p>
      </mat-card-content>
  </mat-card>`,
  styles: []
})
export class HouseCardComponent {
  @Input() house: House;
}
