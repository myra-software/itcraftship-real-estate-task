import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MyMaterialModule } from '../../material.module';
import { HousesListComponent } from './houses-list.component';

describe('HousesListComponent', () => {
  let component: HousesListComponent;
  let fixture: ComponentFixture<HousesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [HousesListComponent],
        imports: [ MyMaterialModule, RouterModule, RouterTestingModule ]
    }).compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(HousesListComponent);
    component = fixture.componentInstance;

    component.houses = [];
    component.houses.push({ id: 1, cityId: 2, onSale: false, price: 150, title: 'Test House 1',
          description: 'House description 1', image: 'http://via.placeholder.com/150x150' });
    component.houses.push({ id: 2, cityId: 2, onSale: true, price: 250, title: 'Test House 2',
      description: 'House description 2', image: 'http://via.placeholder.com/150x150'});
    console.log(component.houses);
    fixture.detectChanges();
  }));

  // TODO(3pts)
  it('should render list of houses', async(() => {
      expect(fixture.nativeElement.querySelectorAll('.mat-row').length).toBe(component.houses.length);

      expect(String(fixture.nativeElement.querySelector('#title_' + component.houses[0].id).textContent)).
      toEqual(component.houses[0].title);

      expect(String(fixture.nativeElement.querySelector('#title_' + component.houses[1].id).textContent)).
      toEqual(component.houses[1].title);
  }));
});
