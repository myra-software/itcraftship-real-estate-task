import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { CitiesService } from '../apis/cities.service';
import { HousesService } from '../apis/houses.service';
import { City, House, HouseFilters } from '../models';

@Component({
  selector: 'app-house-listings',
  template: `      
    <app-houses-filters
      [cities]="cities$ | async"
      [filters]="filters$ | async"
      (filtersChange)="onFiltersChange($event)">
    </app-houses-filters>
    <app-houses-list
      [houses]="houses$ | async">
    </app-houses-list>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `
  ]
})
export class HouseListingsComponent implements OnInit {
  cities$: Observable<City[]>;
  filters$: Observable<HouseFilters>;
  houses$: Observable<House[]>;

  constructor(
    private citiesAPI: CitiesService,
    private houseAPI: HousesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {

      this.cities$ = this.citiesAPI.getCities();
      this.houses$ = this.houseAPI.getHouses({});
      this.filters$ = this.activatedRoute.queryParams;

      this.activatedRoute.queryParams.subscribe(params => {

          const currentFilters: HouseFilters = {};

          // convert url params to proper types in order filtering works fine
          if (typeof params.cityId !== 'undefined') {
              currentFilters.cityId = Number(params.cityId);
          }

          if (typeof params.onSale !== 'undefined') {
              currentFilters.onSale = Boolean(params.onSale === 'true');
          }

          if (typeof params.priceLessThan !== 'undefined') {
              currentFilters.priceLessThan = Number(params.priceLessThan);
          }

          this.houses$ = this.houseAPI.getHouses(currentFilters);
      });

    /*
    1.TODO(1pts)
      Goal: fetch all cities
      Implementation: set this.cities$

    2. TODO(5pts)
      Goal: parse query params stream into filters object
      Implementation: set this.filters$
      Hint:
        query params: real-estate?cityId=3&onSale=false&priceLessThan=244
        parsed value: { cityId: 3, onSale: false, priceLessThan: 244 }

    3. TODO(8pts)
      Goal: fetch all houses matching current filters
      Implementation: set this.houses$
      Hint: this example includes using higher order observables.
            we must switch from the filters$ stream into the houses$ stream.

    */
  }

  onFiltersChange(queryParams: HouseFilters) {

      // do not send empty params
      if (Number(queryParams.cityId) === 0) {
          delete queryParams.cityId;
      }

      if (typeof queryParams.onSale === 'undefined' || Number(queryParams.onSale) === 0) {
          delete queryParams.onSale;
      }

      if (Number(queryParams.priceLessThan) === 0) {
          delete queryParams.priceLessThan;
      }

      this.router.navigate(['.'], { queryParams: queryParams });

    /* TODO(1pts)
      Goal: update URL query params with the new filters
    */
  }
}
